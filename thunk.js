
/*
  The thunk is a shared lazy value.

  It can be forced, then it updates in place. If the result is another thunk,
  the latter is forced as well and its result becomes the result of both.

  If a chain of thunks is produced that way, all of them will be assigned the same
  result of hte last one.
*/

// Placeholder for the thunk being evaluated.
//
let BLACKHOLE = {}

// Shared lazy value.
//
class Lazy {
  constructor(loc, thunk, ready = false) {
    Object.assign(this, {
      loc,    // source map
      thunk,  // a result or a `() => calculation`
      ready   // if true, the thunk is a result
    })
  }
}

// Calculate the lazy value and replace its thunk with a result.
//
// Will update all lazy values returned from original one.
//
let force = (lazy) => {
  let stack = [lazy]

  let loops = 0

  for (;;) {
    // get top element from the stack
    let next = stack[stack.length - 1]

    // if the next element is already evaluated
    // assign it to each waiting lazy value
    // and return its thunk as a result
    if (next.ready) {
      stack.forEach(e => Object.assign(e, next))
      return next.thunk
    }

    loops++

    if (loops >= 10000 && (loops % 10000 == 0)) {
      console.warn(`${loops} iterations in "force()" for ${lazy.loc}`)
    }

    if (loops > 1000000) {
      console.error(`${loops} iterations in "force()" for ${lazy.loc}`)
      throw Error("loop")
    }

    // if the lazy value is already being evaluated, it is a loop
    if (next.thunk == BLACKHOLE) {
      throw Error("loop")
    }

    // extract the thunk and replace it with BLACKHOLE, so we can catch loops
    let f = next.thunk
    next.thunk = BLACKHOLE

    // evaluate the thunk
    let res = f()

    // if the result is lazy as well, add it to the stack and restart
    if (res instanceof Lazy) {
      stack.push(res)
      continue
    }

    // replace the BLACKHOLE with the result
    next.thunk = res
    next.ready = true

    // we will exit in the next iteration in the `if (next.ready)` block
    continue
  }
}

// I hate operator "new".
//
let thunk = (loc, f) => new Lazy(loc, f)

// The self-referencing value.
//
var example = thunk("foo.bar:12:34", () => ({next: example}))

force(example)

// Should print with a [Circular] inside.
//
console.log(example)
