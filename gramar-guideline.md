
# Guidelines

## Indentation

The syntax is entirely indentation-based. Indent is transformed into `「 ` and `」` tokens, which are exchangeable with standard ones. The expression opened with `(` must close with `)` and vice versa.

There two programs are semantically equivalent:
```
let fold-right zero add @loop = \case 
  | Cons x xs -> add x
    loop xs
  | Nil -> zero

let map f = fold-right Nil (before Cons f)

map (add 1) [1,2,3]
```
and (assuming the un-indentator is off)
```
(let fold-right zero add @loop = \case 
  (| Cons x xs -> add x
    (loop xs)
  )
  (| Nil -> zero))

(let map f = fold-right Nil
  (before Cons f))

(map (add 1) [1,2,3])
```

The rule is - if something is possible to do with indentation and it doesn't look silly - we do it with indentation. Also, if there is a shorter and more comprehensible way to do it, it is fine, too.

```
list
  1
  2
  3
```
looks silly, therefore the following syntax is allowed:
```
[1,2,3]
```
Or
```
[,1,2,3]
```
Or
```
[1,2,3,]
```
Or even
```
[,1,2,3,]
```
Because why not? People a struggling with commas for long already.

On the other side, the following doesn't look silly:
```
list
  { a = -1, b = -1 }
  { a =  1, b =  1 }
  { a = -1, b =  1 }
  { a =  1, b = -1 }
```
nor does
```
let funs = list
  rec
    name = "sum"
    body xs = fold-right 0 (+) xs

  rec
    name = "product"
    body xs = fold-right 0 (*) xs
  
  rec
    name = "append"
    body xs ys = fold-right ys Cons xs

```

Notice, that `let` is a part of "definition" here. We can do an `"let" (indent decls) body`, but
if we make it so user prefixes each value-decl with `let`, one can easily notice if it is a definition or not.

## Operators

### Mixfix, lattice

It is possible to do mixfix operators in Agda-style. There are probably some `O(N!)` caveats. AFAIR, they say that - if you made so if only two operators are directly related, they can appear together in one expression - you'll be fine.

```
fixity (+) < (*)

let d = b * b + 4 * a * c
```

But this is too complex, expecially for user. They have to deal with graph of operators which is scattered all over the program in form of `_*_ > _+_` and `if_then_else_ > _+_` and then they discover that `if a then b else c * d` is not a valid expression, because `if_then_else_` and `*` are not directly related.

We can mitigate this by adding operator groups:

```
op group arith
  _+_ < _*_
  arith > bitwise
  arith < access
```

But that will blow up the parser tree and make the priority parser prone to pathological cases.

Another problem of mixfix operators is that code tends to become utterly unreadable.

### Mixfix, full order

~The variant where operators have integral (or, ~better~ worse - floating point) priorities is the same as with op groups. LL(n)-Parser will visit each operator (half of them in average) for each `$foo + $bar` point in program.~

This doesn't sound right.

### Infix ops

Here we can do sections. But, we can also do sections for case where we have no ops, like that:

```
let ?? f x y = f y x
let sect = (?? add 1)
```

### No operators at all

I lean to that variant.

Yes, arithmetics will look a bit ~ugly~ LISP:
```
let d = (+ (* b b) (* 4 a c))

or

(* what the hell is going on here? *)
let d = +
  * b b
  * 4 a c
```

On the other side, if we postulate that we don't have wildcard imports and every name is imported (or universally scoped - a field name), we can compile modules _in any order_ (unless whole program optimisation is used).

That is because we don't need any info from any other module - the names are already here, and operators (and their fixities/priorities) are absent. That is, if we don't have static types.

For the monads, we can make a `do`-syntax and require using it unstead of `>>=`. Yes, one cannot make a `maybe (throwM Err) return =<< gets` oneliners that way, but they aren't good code to begin with.

Do that, instead:
```
do
  it <- gets
  case it of
    | Nothing  -> throwM Err
    | Just it' -> return it
```

## Macroses

We can allow that. The problem is, the language is lazy, so there is no need for them. We can do flow-control-constructs-as-functions already, like that:

```
let if bool selectors = case bool of
  | True  -> selectors.then
  | False -> selectors.else

let and x y = if x rec
  then = y
  else = False
```

## Scope

Static only. Every name used must be either declared, imported or be accessors. This means user will have a lot of qualified code.

```
use module "Data.Function" as { before, id }
use module "Data.List"     as list

let map f =
  list.fold-right list.Nil
    before list.Cons f
```

## Types

Strict, dynamic.