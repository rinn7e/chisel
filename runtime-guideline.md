
# Stuff

The concrete implementations and interfaces can be changed if required.
This is more like a guide, not a requirement.

From my experience with graph reduction machines, we need the following stuff:

0) **Thunks** - delayed values.

   My representation of choice is:

    class Thunk {
      constructor(thunk, ready = false) {
        this.assign({thunk, ready})
      }

      step() {
        let new_thunk = this.thunk()
        if (!this.ready) {
          let new_thunk = new_thunk()

          if (this.thunk instanceof Thunk) {
            this.thunk = () => new_thunk.step()
            this.ready = false
          } else {
            this.ready = true
          }
        }
        return new_thunk
      }
    }

    let thunk = delayed => new Thunk(delayed)
    let eager = val     => new Thunk(val, true)

    let force = val => {
      while (val instanceof Thunk) {
        val = val.step()
      }
      return val
    }

   However, this representation will block on loops, which is not good.

   We can make everything `async`, but I'm not sure - it can make it 20 times slower.

   Or, we can make `force()` `async`, but it does `step()` 20 times and returns a `Promise` that continues to evaluate in batches of 20, for instance. I dunno.

   The key part is that it has the same sematics of `force()`, so if `val` returns another thunk in `.step()`, it in turn evaluates that other thunk as well.
   This part requires an optimisation, because in current state it _is_ a recursion and _will_ occasionally overflow stack.

   Probably, we need more clever `force` function, which keeps an array of thunks for evaluations and throws an error, if thunk being evaluated depends on itself directly.

1) **Curried functions** - either arrow-funcs, or closures (accepts args until full, then invokes the actual code).

   Something like that:

    class Closure {
      constructor(f, arity, ...args) {
        Object.assign(this, {f, args, arity})
      }

      apply(arg) {
        if (this.arity == this.args.length - 1) {
          return this.f(...this.args)
        }

        return new Closure(
          this.f,
          this.arity
          [...this.args, arg],
        )
      }

      apply(...args) {
        ???
      }
    }

    let func = (f, arity) => new Closure(f, arity)

2) **Apply** - the function that creates a thunk that upon evaluation applies a function to its arguments.

    let apply = (f, ...args) => thunk(() => f.apply(...args))

3) **Prisms** - the analogues to constructors in haskell.

    data List
      Cons head tail
      Nil

   This will be represented as 2 prisms in js:

    let Cons = prism(["head", "tail])
    let Nil  = prism([])

   (each call of `prism()` creates a prism with unique id)

   This code:

    let lst = Cons 1 (Cons 2 Nil)

   will be represented as:

    let lst = apply(Cons, 1, apply(Cons, 2, Nil))

   This code:

    case lst of
      Cons x xs ->
        code-using x xs
      Nil ->
        code-on-Nil

   will be represented as:

      match(force(l),
        Cons, ([x, xs]) -> apply(code_using, x, xs),
        Nil,  ([])      -> code_on_Nil,
        "foo.ml:23:45"
      )

   This code:

    case lst of
      Cons { head = x, tail = xs } ->
        code-using x xs
      Nil ->
        code-on-Nil

   will be represented as:

      match(force(l),
        Cons, ({head: x, tail: xs}) -> apply(code_using, x, xs),
        Nil,  []                    -> code_on_Nil,
        "foo.ml:23:45"
      )

   It is better to do it that way if it is possible for it to not consume stack somehow.

   Complex pattern-matches will be represented as combination of `match()` calls.

   The prism that received its last argument should turn into plain JS object.
   The arguments will become named fields. Also, somewhere inside it should track order of fields,
   so it can do `matchIndex`.

   Only the prism that **has not** receive any arguments can be a pattern in `match()`.

   A prism that has no arguments to begin with can be both a pattern to match and a subject of a match.

4) **Objects** - plain JS-opjects will do the trick.

5) **Atomics** - plain JS integers, strings, floats (but not booleans - booleans are prisms).

6) **Lists** - for list literals.

   If we turn list literals to actual lists, it won't be funny. Instead, we can represent
   lists as tree of arrays of elements.
   
    let list = (...args) => new TreeArray(args)
   
   This way when we prepend to a tree, we can do it in list fashion:

    let cons = (elem, tree) => new TreeBranch(new TreeLeaf(elem), tree)

   When we append two lists, we can do it that way:

    let append = (left, right) => new TreeBranch(left, right)

   The "match `Cons x xs ->`" operation can be done by descending to the leftmost branch and pulling an element from that.

   If the branch is an array, we can wrap it into `new Slice(array, 1)`.
   If it is a slice, we can move its left border until it is empty.
   Then we go up, eleminating any empty left-branches.

   This way we'll have fast literal "lists" (which are just an array)

   I can implement it myself in the end, because it only makes lists faster.

   We can live with _just lists_ for now.

7) **Let rec** - some construct to make mutually recursive values possible.

   This code:

    let some p = do
      x  <- p
      xs <- many p
      return [x, ...xs]

    let many p =
      or
        some p
        return []

   will become:

    var some = func(p => thunk(() => ... apply(many, p) ...), 1)
    var many = func(p => thunk(() => ... apply(some, p) ...), 1)

   The `var` is necessary so they see each other due to hoisting. The `thunk` will make sure they are delayed.

8) **Accessors**

    get(smth, "field")
    set(smth, "field", value)
    update(obj, new_obj)

   The latter will be, probably

    let update (x, y) = Object.assign({}, x, y)

   Because all values in our language (except very special ones) are immutable.

9) **IO**

   We need to do IO. For that, we can use either `Promise`, or turn IO code into `async`/`await`. The **Strict evaluator** must inspect the result of the `Promise` and `force()` it as well.

10) **Interop**/**Strict evaluator**

   The JS values will be already compatible with our runtime.
   We might want to send stuff to JS, though.

   For that, we need a `toJs` method that will walk over lazy values and _results of their evaluation_ and `force()` all their fields.
   This method should detect if `force()` of some thunk calls `force()` for the same thunk,
   and throw some `Loop` exception.

   This method, also, should do

    let wrap_fun = f => (...args) => toJS(f(...args))

   to functions.

   Also, this method should not consume stack - it should store the queue/stack in the array.
